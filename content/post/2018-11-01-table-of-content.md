---
draft: true
title: Inserting a Table of Content
thumbnail: https://i0.wp.com/texblog.org/Wordpress/wp-content/uploads/2013/04/toc-depth1.png
author:
  - Lars Blumberg
date: '2018-11-01'
categories:
  - Example
  - Hugo
toc: true
---

Use the attribute `toc: true` to insert a Table of Content.

# Section 1

This is section 1

## Section 1.1

This is section 1.1

### Section 1.1.1

This is section 1.1.1
