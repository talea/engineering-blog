---
draft: true
title: Using a custom post summary
thumbnail: https://www.bachelorprint.ch/wp-content/uploads/2017/05/Summary-Zusammenfassung-schreiben.jpg
author:
  - Lars Blumberg
date: '2018-11-01'
categories:
  - Example
  - Hugo
tags:
  - blog
description: "This article explains how to use a custom summary which replaces the automatically generated summary."
---

By default the article summary on the homepage contains the first 200 letters extracted from all paragraphs of an article. If you are not satisfied with the automatic summary, you can specify the `description` option in the metadata.
