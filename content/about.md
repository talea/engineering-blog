---
title: About Talea Engineering
---

We are the Talea Engineering Team, a bunch of awesome and diverse engineers which thrives to build innovative products for the home healthcare market. We are lucky enough to construct our products on a greenfield which allows us to use latest and greatest technologies such GraphQL, ReactJS, Kotlin or Python 3.8. On this blog we are sharing our passion, experiences and findings to give back to our fellow engineers.
